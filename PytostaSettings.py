class PytostaSettings:
    ''' Settings class for Pytosta '''
    def __init__ (self):
            ''' Initialize settings '''
            # site info
            # Leave the root_url blank to use the local output path 
            self.root_url = ''
            self.root_url = 'http://localhost:8000'
            self.site_name = 'PytoSta'
            self.root = ''
            self.logo_image = 'logo.png'

            # site
            self.index = 'index.html'
            self.contents_file = 'contents.html'
            self.default_text = 'Dai diamanti non nasce niente...'
            
            # content dir and templates dir
            self.content_dir = 'content'
            self.template_dir = 'templates'
            # Dir containing static content (e.g. css, images etc.) 
            self.static_dir = 'static'
            # If true all content in included dirs file will be copied to the 
            # same directory in the output, e.g. an image, document etc.
            # Otherwise only the contents file will be processed.
            self.copy_files = True
           
            # menu
            self.menu_file = 'menu.txt'
            self.extra_links = 'extralinks.txt'
            # This will explicity add a 'home' link to the menu. The contents
            # file will be in the root of the content_dir.
            self.add_home = False

            # CSS
            self.main_css = 'pytosta.css'
