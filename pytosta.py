#!/usr/bin/env python
import os
import logging
import distutils.core
from copy import deepcopy
import argparse

import PytostaSettings
try:
    from lxml import etree
    from lxml import html
except ImportError:
    print("PytoSta needs lxml. See http://lxml.de/") 

def make_dir_strc(root):
    ''' Generate a dictionary with a 2-level directory tree structure '''
    dirstrc_dict = {}
    dir_list1 = [ x for x in os.listdir(root)
            if os.path.isdir(os.path.join(root, x)) ]
    for d in dir_list1:
        full = os.path.join(root, d)
        dir_list2 = [y for y in os.listdir(full)
                if os.path.isdir(os.path.join(full, y)) ]
        dirstrc_dict[d] = dir_list2
    return dirstrc_dict

def file_copy(src_dir, dst_dir, exclude_list):
    ''' Used to copy files from source to destination if needed. Source and dst
    dir should already be absolute paths! Exclude list are (non absolute) file-
    names which won't be copied (e.g. menu.txt, contentes.html)'''
    copied_list = []
    file_list = [
            f for f in os.listdir(src_dir)
            if os.path.isfile(os.path.join(src_dir, f))
            ]
    for x in exclude_list:
        try:
            file_list.remove(x)
        except:
            pass
    for f in file_list:
        if f[-1:] == '~' or f[0] == '.':
            continue
        src_file = os.path.join(src_dir, f)
        distutils.dir_util.mkpath(dst_dir)
        res = distutils.file_util.copy_file(src_file,
                os.path.join(dst_dir, f),
                update=1)
        copied_list.append(res)

class Pytosta:
    ''' Main class for PytoSta '''
    def __init__ (self, content_dir='content', out_dir='output'):
        ''' Initialise settings and start '''
        self.log = logging.getLogger('pytosta')
        self.log.setLevel(logging.INFO)
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        formatter = logging.Formatter('[%(levelname)s]: %(message)s')
        ch.setFormatter(formatter)
        self.log.addHandler(ch)
        self.log.info("PytoSta initialized")
        if content_dir != None:
            self.content_dir = content_dir
        else:
            self.content_dir = 'content'
        if out_dir != None:
            self.out_dir = out_dir
        else:
            self.out_dir = 'output'
        # Default dirs names (relative to output dir)
        self.default_dirs = {'css': '.css',
                'images': '.images'
                }
        self.css_dir = os.path.join(self.out_dir, self.default_dirs['css'])
        self.settings = PytostaSettings.PytostaSettings()
        self.site_url = self.settings.root_url
        if self.settings.root_url == '':
            self.site_url = '//'
        self.struct_dic = make_dir_strc(self.content_dir)
        #print self.struct_dic
        self.current_path = ''
        self.index_fmame = self.settings.index        

        template_dir = self.settings.template_dir
        self.head_tpl = self.read_template(os.path.join(template_dir, 'head.html'))
        self.body_tpl = self.read_template(os.path.join(template_dir, 'body.html'))
        self.foot_tpl = self.read_template(os.path.join(template_dir, 'footer.html'))

    def make_default_dirs(self):
        ''' Create the output dir and other default dirs (e.g. .css etc.) if
        needed '''
        if not os.path.exists(os.path.abspath(self.out_dir)):
            try:
                os.makedirs(self.out_dir)
            except OSError:
                print(("Error creating dir %s!") % (self.out_dir))
        for k in self.default_dirs.keys():
            create_dir = os.path.join(self.out_dir, self.default_dirs[k])
            if not os.path.exists(create_dir):
                try:
                    os.makedirs(create_dir)
                except OSError:
                    print(("Error creating dir %s!") % (create_dir))

    def copy_static(self):
        ''' Copy default dirs from the static directory to the output root.
        If for example there is a dir ./static/.css and 'css' is defined in the
        self.default_dirs dictionary the corresponding dir name will be created
        in the output root and all files copied in there. Uses distutils.dir_util
        as suggested here: http://stackoverflow.com/a/15034373
        '''
        self.log.info("Copying static content")
        static_dir = self.settings.static_dir
        for k in self.default_dirs.keys():
            from_dir = os.path.join(static_dir, k)
            to_dir = os.path.join(self.out_dir, self.default_dirs[k])
            distutils.dir_util.copy_tree(from_dir, to_dir, update=1)


    def read_template(self, fname):
        try:
            t = etree.parse(fname)
        except etree.XMLSyntaxError:
            f = open(fname)
            t = html.fragments_fromstring(f.read())
            f.close()
        return t

    def make_head(self, title_string):
        ''' Make a page header (<head>) section'''
        hd = deepcopy(self.head_tpl).getroot()
        title = hd.xpath('title')[0]
        title.text = title_string
        for f in os.listdir(self.css_dir):
            # to avoid adding vim swap and backup files

            if (f.find('.css') >= 0) and (f[-1] != '~') and (f.find('.swp') < 0):
                css_rel = etree.fromstring(
                        '    <link rel="stylesheet" type="text/css" />'
                        )
                css_href = os.path.join(self.site_url, self.default_dirs['css'], f)
                css_rel.attrib['href'] = css_href
                hd.append(css_rel)
        return hd

    def make_menu(self, body_element):
        ''' Make the menu structure and generate the html for it '''
        nav_el = body_element.xpath("nav[@id='nav_root']")[0]
        ul_nav_el = etree.fromstring('<ul id="nav_ul" />')
        
        # --- Level 1
        #   --- Check if there's a menu file for level 1
        menu1_file_path = os.path.join(
                self.content_dir, self.settings.menu_file
                )
        if os.path.exists(menu1_file_path):
            menu_file1 = open(menu1_file_path)
            level_1_dirlist = [line.rstrip() for line in menu_file1.readlines()]
            menu_file1.close()
            self.log.info("Using menu file for level 1: " + str(level_1_dirlist))
        else:
            level_1_dirlist = self.struct_dic.keys()
            level_1_dirlist.sort()
        id_counter = 0
        for dir1 in level_1_dirlist:
            submenu_count = 0
            # exclude hidden dirs from the menu by default
            if dir1[0] == '.':
                continue
            li_1_el = etree.fromstring('<li class="nav_li_01" />')
            a_1_class_string = "nav_a_01"
            a_1_el = etree.fromstring('<a />')
            href = os.path.join(self.site_url, dir1)
            a_1_el.attrib['href'] = href
            target_id = 'menu_id_' + str(id_counter)
            a_1_el.text = dir1.replace('_',' ')
            li_1_el.append(a_1_el)
            
            # --- Level 2
            #   -- Check if there's a menu file for level 2
            menu2_file_path = os.path.join(
                    self.content_dir, dir1, self.settings.menu_file
                    )
            if os.path.exists(menu2_file_path):
                menu_file2 = open(menu2_file_path)
                level_2_dirlist = [line.rstrip() for line in menu_file2.readlines()]
                menu_file2.close()
                self.log.info("Using menu file for level 2: " + str(level_2_dirlist))
            else:
                level_2_dirlist = self.struct_dic[dir1]
                level_2_dirlist.sort()
        
            if level_2_dirlist:
                ul_2_el = etree.fromstring('<ul class="nav_ul_02" />')
                li_1_el.append(ul_2_el)
                ul_2_el.attrib['id'] = target_id 
            for dir2 in level_2_dirlist:
                if dir2[0] == '.':
                    continue

                li_2_el = etree.fromstring('<li class="nav_li_02" />')
                a_2_el = etree.fromstring('<a class="nav_a_02" />')
                href = os.path.join(self.site_url, dir1, dir2)
                a_2_el.attrib['href'] = href
                a_2_el.text = dir2.replace('_',' ')
                li_2_el.append(a_2_el)              
                ul_2_el.append(li_2_el)
                submenu_count = submenu_count + 1
            #if there is at least a submenu entry add a class to the level 1
            if submenu_count > 1:
                a_1_class_string = a_1_class_string + " has_submenu"
            a_1_el.attrib['class'] = a_1_class_string

            ul_nav_el.append(li_1_el)
            id_counter = id_counter + 1

        nav_el.append(ul_nav_el)

    def make_footer(self, body_element):
        ''' Make a footer <div> '''
        
    def make_body(self, content_file, the_dir, title_string):
        ''' Import the content from the content file and add it to the page
        body'''
        bd = deepcopy(self.body_tpl).getroot()
        self.make_menu(bd) 
        # header div
        div_head_el = bd.xpath('//div[@id="header_div"]')[0]
        h1_el = etree.fromstring('<h1 />')
        a_el = etree.fromstring('<a href="/" class="home_url" />')
        img_el = etree.fromstring('<img class="logo_img" alt="" />')
        a_el.attrib['href'] = self.site_url
        logo_src = os.path.join(self.site_url,
                self.default_dirs['images'],
                'logo.png')
        img_el.attrib['src'] = logo_src
        a_el.append(img_el)
        img_el.tail = self.settings.site_name
        h1_el.append(a_el)
        div_head_el.append(h1_el)
        add_default = False
        try:
            cf = open(content_file)
            content_el_list = html.fragments_fromstring(cf.read())
            cf.close()
        except etree.XMLSyntaxError, e:
            add_default = True
            self.log.warn("lxml error processing %s: %s", content_file, e)
        except IOError, e:
            self.log.warn("Content file doesn't exist: %s", content_file)
            if os.path.exists(os.path.join(the_dir, 'index.html')):
                add_default = False
                self.log.warn('No content file, but found an "index.html" file in dir')
                return None
            else:
                add_default = True
                self.log.warn("Adding default content to %s", content_file)
                content_el = etree.fromstring(('<p>%s</p>') % (self.settings.default_text))
                content_el_list = [content_el]

        div_content_el = bd.xpath('//div[@id="content_div"]')[0]
        h1_title_el = div_content_el.xpath('//h1[@class="page_title"]')[0]
        try:
            title_file = open(os.path.join(the_dir, 'title.txt'))
            title_string = title_file.read()
            title_file.close()
        except:
            pass
        h1_title_el.text = title_string
        for tag in content_el_list: 
            div_content_el.append(tag)
        
        div_footer_el = bd.xpath('//div[@id="footer"]')[0]
        foot = deepcopy(self.foot_tpl)
        try:
            foot_el = foot.getroot()
            div_footer_el.append(foot_el)
        except AttributeError:
            for tag in foot:
                div_footer_el.append(tag)

        return bd

    def make_page(self, input_dir):
        ''' Make a single page '''
        title = input_dir.replace('_',' ')
        if (os.path.split(title)[1]) != '':
            title = os.path.split(title)[1]
        head_el = self.make_head(title)
        this_dir = os.path.join(self.content_dir, input_dir)
        # TODO Allow for other extensions
        cont_file = os.path.join(this_dir, self.settings.contents_file)
        body_el = self.make_body(cont_file, this_dir, title)
        if body_el == None:
            return -2
        html_el = etree.fromstring('<html lang="en"></html>')
        html_el.append(head_el)
        html_el.append(body_el)
        html_string = '<!doctype html>'
        html_string += etree.tostring(html_el, method='html', pretty_print = True)
        out_dir = os.path.join(self.out_dir, input_dir)
        try:
            os.makedirs(out_dir)
        except OSError:
            pass
            '''
            self.log.error("Error creating dirs %s", out_dir)
            return -1
            '''
        out_file = open(os.path.join(out_dir, self.index_fmame), 'w+')
        out_file.write(html_string)
        out_file.close()
        return 0

    def make_website(self):
        ''' Make thy website. Use content_dir as the root dir and generate the
        site in out_dir '''
        self.make_default_dirs()
        self.copy_static()
        out_abs = os.path.abspath(self.out_dir)
        excl_cpy_list = [
                self.settings.menu_file,
                self.settings.contents_file,
                'title.txt'
                ]
        # ---- Level 0 -----
        self.log.info('- Processing %s', self.content_dir)
        self.make_page('')
        if self.settings.copy_files:
            self.log.info(' - Copying files...')
            file_copy(
                    os.path.abspath(self.content_dir + os.path.sep),
                    os.path.abspath(self.out_dir),
                    excl_cpy_list
                    )
        # --------- Level 1 --------------
        for dir1 in self.struct_dic.keys():
            self.log.info("|- Processing %s", dir1)
            self.make_page(dir1)
            if self.settings.copy_files:
                src1_abs = os.path.join(os.path.abspath(self.content_dir), dir1)
                dst1_abs = os.path.join(out_abs, dir1)
                self.log.info(' |- Copying files...' )
                file_copy(src1_abs, dst1_abs, excl_cpy_list)
            # ---------- Level 2 ------------
            for dir2 in self.struct_dic[dir1]:
                self.log.info("  |- Processing %s/%s", dir1, dir2)
                self.make_page(os.path.join(dir1,dir2))
                if self.settings.copy_files:
                    src2_abs = os.path.join(
                            os.path.abspath(self.content_dir),
                            dir1,
                            dir2
                            )
                    dst2_abs = os.path.join(out_abs, dir1, dir2)
                    self.log.info('   |- Copying files...' )
                    file_copy(src2_abs, dst2_abs, excl_cpy_list)



parser = argparse.ArgumentParser()
parser.add_argument("-c", "--cdir", help="Input contents directory",
        action="store", dest="content_dir_arg")
parser.add_argument("-o", "--outdir", help="Output dir",
        action="store", dest="output_dir_arg")
args = parser.parse_args()
p = Pytosta(content_dir = args.content_dir_arg, out_dir = args.output_dir_arg)
p.make_website()
